package edu.ifpb.dac;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.TableGenerator;

/**
 *
 * @author Ricardo Job
 */
@Entity
@TableGenerator(name = "tab_func",
        table = "GeradorFuncionario",
        pkColumnName = "chave_func",
        valueColumnName = "valor_func",
        pkColumnValue = "func_id",
        allocationSize = 1)
public class Funcionario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE,generator = "tab_func")
    private int id;
    private String nome;
    private String cpf;

    public Funcionario() {
    }

    public Funcionario( String nome, String cpf) {
        
        this.nome = nome;
        this.cpf = cpf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return "Professor{" + "id=" + id + ", nome=" + nome + ", cpf=" + cpf + '}';
    }

}
