package edu.ifpb.dac;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Ricardo Job
 */
public class App {

    public static void main(String[] args) {
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("configurcao");
//        EntityManager em = emf.createEntityManager();

        Aluno pessoa = new Aluno("Dona Florinda", 30, "9856");
        pessoa.setEndereco(new Endereco("Vila do Chaves 3"));
        
        Dao daoJPA = new DaoJPA();
        
        //daoJPA.save(pessoa);
        
        
       Aluno a = (Aluno) daoJPA.find(Aluno.class, "9856");
//       Endereco end = (Endereco) daoJPA.find(Endereco.class, 1);
//        System.out.println(a);
//        System.out.println(end);
//        
//        a.setNome("Seu Madruga");
       a.getEndereco().setRua("Qualquer");
       
       
       daoJPA.update(a);
        System.out.println("Atualizado: " + a);
        /**Utilizar Gerador de automático*/
//        persist(em, pessoa);
//        listar(em);
        /**Utilizar Gerador de sequência*/
//        Professor p = new Professor("Job", "123");
//        persist(em, p);
        /**Utilizar Gerador de Table*/
//        Funcionario func = new Funcionario("Ana", "256");
//        persist(em, func);
          /**Utilizar Gerador de Identity*/
//        Turma turma = new Turma("DAC");
//        persist(em, turma);
    }

}
