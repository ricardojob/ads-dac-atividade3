package edu.ifpb.dac;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Ricardo Job
 */
@Entity
@Table(name = "Pessoa")
public class Aluno implements Serializable {

    @Id
    private String cpf;

    private String nome;
    private int idade;

    
    @OneToOne(cascade = CascadeType.ALL)   
    private Endereco endereco;
    
    public Aluno() {
    }

    public Aluno(String nome, int idade, String cpf) {
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return "Pessoa{ nome: " + nome +  " cpf:" + cpf +  " idade: " + idade + '}';
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    
}
